(* ::Package:: *)

(* :Name: OptimalControl` *)

(* :Title: Utility functions for PDE-based inpainting with the Laplacian. *)

(* :Author: Laurent Hoeltgen *)

(* :Copyright: Copyright 2014-2016, Laurent Hoeltgen <hoeltgen@b-tu.de> *)

(* :License:
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

(* :Summary:
	Provides some support function for PDE-based inpainting with the Laplacian.
*)

(* :References:

	Hoeltgen L., Setzer S., and Weickert J. (2013),
	"An Optimal Control Approach to Find Sparse Data for Laplace Interpolation",
	In: Energy Minimization Methods in Computer Vision and Pattern Recognition.
	LNCS Vol. 8081, Springer Berlin.

	Hoeltgen L., and Weickert J. (2015),
	"Why does non-binary mask optimisation work for diffusion-based image compression?",
	In: Energy Minimization Methods in Computer Vision and Pattern Recognition.
	LNCS Vol. 8932, pp. 85-98. Springer.
*)

(* :Context: Compression`OptimalControl` *)

(* :Package Version: 0.1 *)

(* :Usage:
	On[Assert];
	Needs["Compression`OptimalControl`", "~/<some dirs>/OptimalControl.m"];
*)

(* :Todo:
*)


BeginPackage["Compression`OptimalControl`"]


IM::usage="Sparse Identity Matrix."
SyntaxInformation[IM]={"ArgumentsPattern"->{_}};

FD1D::usage="Forward Differences in 1D."
SyntaxInformation[FD1D]={"ArgumentsPattern"->{_,_}};

FD2D::usage="Forward Differences in 2D."
SyntaxInformation[FD2D]={"ArgumentsPattern"->{_,_,_}};

BD1D::usage="Backward Differences in 1D."
SyntaxInformation[BD1D]={"ArgumentsPattern"->{_,_}};

BD2D::usage="Backward Differences in 2D."
SyntaxInformation[BD2D]={"ArgumentsPattern"->{_,_,_}};

CD1D::usage="Central Differences in 1D."
SyntaxInformation[CD1D]={"ArgumentsPattern"->{_,_}};

CD2D::usage="Central Differences in 2D."
SyntaxInformation[CD2D]={"ArgumentsPattern"->{_,_,_}};

L21D::usage="Laplacian in 1D."
SyntaxInformation[L21D]={"ArgumentsPattern"->{_,_}};

L22D::usage="Laplacian in 2D."
SyntaxInformation[L22D]={"ArgumentsPattern"->{_,_,_}};

CM::usage="Diagonal matrix for mask construction."
SyntaxInformation[CM]={"ArgumentsPattern"->{{__}}};

InpM1D::usage="Inpainting matrix in 1D."
SyntaxInformation[InpM1D]={"ArgumentsPattern"->{{__},_}};

InpM2D::usage="Inpainting matrix in 2D."
SyntaxInformation[InpM2D]={"ArgumentsPattern"->{{__},_,_,_}};


Unprotect[IM, FD1D, FD2D, BD1D, BD2D, CD1D, CD2D, L21D, L22D, CM, InpM1D, InpM2D];


Begin["`Private`"]


Clear[IM];
IM[n_Integer:1]:=SparseArray[{{i_,i_}->1}, {n,n}]


Clear[FD1D];
FD1D[n_Integer:1, h_Real:1]:=SparseArray[{{n,n}->0, {i_,i_}->-1/h, {i_,j_}/;(j==i+1)->1/h}, {n,n}]


Clear[FD2D];
FD2D[nr_Integer:1, nc_Integer:1, h_Real:1]:=KroneckerProduct[IM[nr], FD1D[nc, h]]


Clear[BD1D];
BD1D[n_Integer:1, h_Real:1]:=SparseArray[{{1,1}->0, {i_,i_}->1/h, {i_,j_}/;(j==i-1)->-1/h}, {n,n}]


Clear[BD2D];
BD2D[nr_Integer:1, nc_Integer:1, h_Real:1]:=KroneckerProduct[IM[nr], BD1D[nc,h]]


Clear[CD1D];
CD1D[n_Integer:1, h_Real:1]:=SparseArray[{{1,1}->-1/(2*h), {n,n}->1/(2*h), {i_,j_}/;(j==i+1)->1/(2*h), {i_,j_}/;(j==i-1)->-1/(2*h)}, {n,n}]


Clear[CD2D];
CD2D[nr_Integer:1, nc_Integer:1, h_Real:1]:=KroneckerProduct[IM[nr], CD1D[nc,h]]


Clear[L21D];
L21D[n_Integer:1, h_Real:1]:=SparseArray[{{1,1}->-1/h^2, {n,n}->-1/h^2, {i_,i_}->-2/h^2, {i_,j_}/;(Abs[i-j]==1)->1/h^2}, {n,n}]


Clear[L22D];
L22D[nr_Integer:1, nc_Integer:1, h_Real:.0]:=KroneckerProduct[L21D[nc,h], IM[nr]]+KroneckerProduct[IM[nc], L21D[nr,h]]


Clear[CM];
CM[c_]:=SparseArray[{i_,i_}:>c[[i]],Length[c]{1,1}]


Clear[InpM1D];
InpM1D[c_, h_Real:1.0]:=CM[c]-(IM[Length[c]]-CM[c]).L21D[Length[c],h]


Clear[InpM2D];
InpM2D[c_, nr_Integer:1, nc_Integer:1, h_Real:1.0]:=CM[c]-(IM[Length[c]]-CM[c]).L22D[nr,nc,h]


End[]
SetAttributes[
{ IM, FD1D, FD2D, BD1D, BD2D, CD1D, CD2D, L21D, L22D, CM, InpM1D, InpM2D },
{ Protected, ReadProtected }
];
EndPackage[]
