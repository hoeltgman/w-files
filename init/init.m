(** User Mathematica initialization file **)
(** On Mac OS X save this file under ~/Library/Mathematica/Kernel/
    to autoload the packages. **)
Needs["Compression`OptimalControl`",
	"~/dev/w-files/OptimalControl/OptimalControl.m"];
Needs["Compression`FreeKnot`",
	"~/dev/w-files/FreeKnot/L1FreeKnot.m"];
