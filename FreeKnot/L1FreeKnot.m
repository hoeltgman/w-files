(* ::Package:: *)

(* :Name: FreeKnot` *)

(* :Title: Utility functions for the 1D free knot problem. *)

(* :Author: Laurent Hoeltgen *)

(* :Copyright: Copyright 2011-2016, Laurent Hoeltgen <hoeltgen@b-tu.de> *)

(* :License:
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

(* :Summary:
	This package provides a number of supporting functions for solving the
	free knot problem in 1D.
*)

(* :References:
	Hoeltgen, L.; Mainberger, M.; Hoffmann, S.; Weickert, J.; Tang, C. H.;
		Setzer, S.; Johannsen, D.; Neumann, F. & Doerr
	Optimising Spatial and Tonal Data for PDE-based Inpainting
	Variational Methods, De Gruyter, 2016
*)

(* :Context: Compression`FreeKnot` *)

(* :Package Version: 0.1 *)

(* :Usage:
	On[Assert];
	Needs["Compression`FreeKnot`", "~/<some dirs>/L1FreeKnot.m"];
*)

(* :Todo:
	Check input parameters through patterns.
	Improve the handling of the options.
*)


BeginPackage[ "Compression`FreeKnot`" ]


ErrorInterp::usage="ErrorInterp[f,x]
Returns the L1 error between f and its piecewise linear interpolation with support points x.
Note that f is assumed to be a (strictly) convex function.";
SyntaxInformation[ErrorInterp]={"ArgumentsPattern"->{_,_}};

ErrorInterpLoc::usage="ErrorInterpLoc[f,x]
Returns the local L1 error between f and its piecewise linear interpolation with support points x.
Note that f is assumed to be a (strictly) convex function.";
SyntaxInformation[ErrorInterpLoc]={"ArgumentsPattern"->{_,_}};

ErrorApprox::usage="ErrorApprox[f,x] returns the L1 error between f and its piecewise linear approximation with knots x.";
SyntaxInformation[ErrorApprox]={"ArgumentsPattern"->{_,_}};

ErrorApproxLoc::usage="ErrorApproxLoc[f,x] returns the local L1 error between f and its piecewise linear approximation with knots x.";
SyntaxInformation[ErrorApproxLoc]={"ArgumentsPattern"->{_,_}};

FreeKnotInterp::usage="FreeKnotInterp[f,FpI,a,b,Num,It]
Returns the optimal knot distribution for f on the interval [a,b]. f is the considered function,
FpI the inverse of the derivative of f. Num is the number of inner points, e.g. of free knots
and It the number of iterations to perform. The numbers a and b are the lower and upper bound
of the considered interval.";
SyntaxInformation[FreeKnotInterp]={"ArgumentsPattern"->{_,_,_,_,_,_,OptionsPattern[{Init->"uniform"}]}};

FreeKnotApprox::usage="FreeKnotApprox[f,a,b,Num,It]
Returns the optimal knot distribution for f on the interval [a,b]. f is the considered function.
Num is the number of inner points, e.g. of free knots and It the number of iterations to perform.
The numbers a and b are the lower and upper bound of the considered interval.";
SyntaxInformation[FreeKnotApprox]={"ArgumentsPattern"->{_,_,_,_,_,OptionsPattern[{Init->"uniform"}]}};


L1FreeKnot::BadOpt="The passed option must either be random or uniform. Fallback is random." 


Unprotect[ErrorInterp, ErrorInterpLoc, ErrorApprox, FreeKnotInterp, FreeKnotApprox];


Begin["`Private`"]


ErrorInterp[f_,x_?VectorQ]:=Module[{i,t},
	Return[
		Total[Table[0.5*(x[[i+1]]-x[[i]])*(f[x[[i+1]]]+f[x[[i]]]),{i,1,Length[x]-1}]]-NIntegrate[f[t],{t,x[[1]],x[[-1]]}]
	];
];


ErrorInterpLoc[f_,x_?VectorQ]:=Module[{i,t},
	Return[
		Table[0.5*(x[[i+1]]-x[[i]])*(f[x[[i+1]]]+f[x[[i]]])-NIntegrate[f[t],{t,x[[i]],x[[i+1]]}],{i,1,Length[x]-1}]
	];
];


ErrorApprox[f_,x_?VectorQ]:=Module[{A,B,C,x1,x2,i,Err},
	Err=0;
	For[i=1,i<=Length[x]-1,i++,
		x1=3/4*x[[i]]+1/4*x[[i+1]];
		x2=1/4*x[[i]]+3/4*x[[i+1]];
		A=Integrate[f[t]-(f[x2]-f[x1])/(x2-x1)*(t-x1)-f[x1],{t,x[[i]],3/4*x[[i]]+1/4*x[[i+1]]}];
		B=Integrate[(f[x2]-f[x1])/(x2-x1)*(t-x1)+f[x1]-f[t],{t,3/4*x[[i]]+1/4*x[[i+1]],1/4*x[[i]]+3/4*x[[i+1]]}];
		C=Integrate[f[t]-(f[x2]-f[x1])/(x2-x1)*(t-x1)-f[x1],{t,1/4*x[[i]]+3/4*x[[i+1]],x[[i+1]]}];
		Err=Err+A+B+C;
	];
	Return[Err];
];


ErrorApproxLoc[f_,x_?VectorQ]:=Module[{A,B,C,x1,x2,i,Err},
	Err={};
	For[i=1,i<=Length[x]-1,i++,
		x1=3/4*x[[i]]+1/4*x[[i+1]];
		x2=1/4*x[[i]]+3/4*x[[i+1]];
		A=Integrate[f[t]-(f[x2]-f[x1])/(x2-x1)*(t-x1)-f[x1],{t,x[[i]],3/4*x[[i]]+1/4*x[[i+1]]}];
		B=Integrate[(f[x2]-f[x1])/(x2-x1)*(t-x1)+f[x1]-f[t],{t,3/4*x[[i]]+1/4*x[[i+1]],1/4*x[[i]]+3/4*x[[i+1]]}];
		C=Integrate[f[t]-(f[x2]-f[x1])/(x2-x1)*(t-x1)-f[x1],{t,1/4*x[[i]]+3/4*x[[i+1]],x[[i+1]]}];
		Err=Append[Err,A+B+C];
	];
	Return[Err];
];


FreeKnotInterp[f_,FpI_,a_?(Function[v,NumericQ[v]&&(Im[v]==0)]),b_?(Function[v,NumericQ[v]&&(Im[v]==0)]),Num_Integer?(Function[v,v>=1]),It_Integer?(Function[v,v>=0]),OptionsPattern[{Init->"uniform"}]]:=Module[{x,t,i,x0},
	If[Not[MemberQ[{"random","uniform"},OptionValue[Init]]],
		Message[L1FreeKnot::BadOpt];
	];
	If[OptionValue[Init]=="uniform",
		x0=N[Table[a+i*(b-a)/(Num+1),{i,0,Num+1}]],
		x0=Sort[Union[RandomReal[{a,b},Num],{a,b}]];
	];		
	x=N[x0];
	For[t=1,t<=It,t++,
		x[[2;;Num+1;;2]]=Map[FpI,(Map[f,x[[3;;Num+2;;2]]]-Map[f,x[[1;;Num;;2]]])/(x[[3;;Num+2;;2]]-x[[1;;Num;;2]])];
		x[[3;;Num+1;;2]]=Map[FpI,(Map[f,x[[4;;Num+2;;2]]]-Map[f,x[[2;;Num;;2]]])/(x[[4;;Num+2;;2]]-x[[2;;Num;;2]])];
	];
	Return[{x0,x}]
];


FreeKnotApprox[f_,a_?(Function[v,NumericQ[v]&&(Im[v]==0)]),b_?(Function[v,NumericQ[v]&&(Im[v]==0)]),Num_Integer?(Function[v,v>=1]),It_Integer?(Function[v,v>=0]),OptionsPattern[{Init->"uniform"}]]:=Module[{x0,x,i,k,\[Xi],Dpf,Df},
	If[Not[MemberQ[{"random","uniform"},OptionValue[Init]]],
		Message[L1FreeKnot::BadOpt];
	];
	If[OptionValue[Init]=="uniform",
		x0=N[Table[a+i*(b-a)/(Num+1),{i,0,Num+1}]],
		x0=Sort[Union[RandomReal[{a,b},Num],{a,b}]];
	];
	x=x0;
	\[Xi]=Table[0,{Num+1},{2}];
	For[k=1,k<=It,k++,
		\[Xi][[All,1]]=0.75*x[[1;;-2]]+0.25*x[[2;;-1]];
		\[Xi][[All,2]]=0.25*x[[1;;-2]]+0.75*x[[2;;-1]];
		Dpf=(Map[f,\[Xi][[2;;-1,2]]]-Map[f,\[Xi][[2;;-1,1]]])/(\[Xi][[2;;-1,2]]-\[Xi][[2;;-1,1]]);
		Df=(Map[f,\[Xi][[1;;-2,2]]]-Map[f,\[Xi][[1;;-2,1]]])/(\[Xi][[1;;-2,2]]-\[Xi][[1;;-2,1]]);
		x[[2;;-2]]=(\[Xi][[1;;-2,1]]*Df-\[Xi][[2;;-1,1]]*Dpf-Map[f,\[Xi][[1;;-2,1]]]+Map[f,\[Xi][[2;;-1,1]]])/(Df-Dpf);
	];
	Return[{x0,x}];
];


End[]


SetAttributes[
{ ErrorInterp, ErrorInterpLoc, ErrorApprox, ErrorApproxLoc, FreeKnotInterp, FreeKnotApprox },
{ Protected, ReadProtected }
];


EndPackage[]
