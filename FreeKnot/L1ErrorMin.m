(* ::Package:: *)

(* :Name: ErrorMin` *)

(* :Title: Error Minimisation. *)

(* :Author: Laurent Hoeltgen *)

(* :Copyright: Copyright 2011-2016, Laurent Hoeltgen <hoeltgen@b-tu.de> *)

(* :License:
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

(* :Summary:
	Contains various Stuff related to the error minimisation in the L1 Norm
	of piece wise interpolation.
*)

(* :Context: Compression`L1ErrorMin` *)

(* :Package Version: 0.1 *)

(* :History: Version 0.1 by Laurent Hoeltgen, January 2014. *)

(* :Keywords: Image compression, Error minimisation *)

(* :Source: *)

(* :Mathematica Version: 9.0 *)

(* :Limitation: *)

(* :Discussion: *)


BeginPackage[ "Compression`ErrorMin`" ]


LinInterp::usage="LinInterp[x,p,f] is the piecewise linear interpolat with support points p of the function f evaluated at x.";
SyntaxInformation[LinInterp]={"ArgumentsPattern"->{_,_,_}};


L1Error::usage="L1Error[f,p] returns the L1 error between f and its piecewise linear interpolation with support points p.";
SyntaxInformation[L1Error]={"ArgumentsPattern"->{_,_}};


L2Error::usage="L1Error[f,p] returns the L2 error between f and its piecewise linear interpolation with support points p.";
SyntaxInformation[L2Error]={"ArgumentsPattern"->{_,_}};


FindP::usage="FindP[a,b,f,fpI,Num,iter] determines the optimal support point distribution of f using piecewise linear Interpolation.";
SyntaxInformation[FindP]={"ArgumentsPattern"->{_,_,_,_,_,_}};


ListP::usage="ListP[p,f] returns the point distribution p such that it can be plotted with Listplot.";
SyntaxInformation[ListP]={"ArgumentsPattern"->{_,_}};


GradErrL1::usage="GradErrL1[f,p] is the gradient of the L1 error functional.";
SyntaxInformation[GradErrL1]={"ArgumentsPattern"->{_,_}};


HessErrL1::usage="HessErrL1[f,p] is the Hessian matrix of the L1 error functional.";
SyntaxInformation[HessErrL1]={"ArgumentsPattern"->{_,_}};


Unprotect[LinInterp, L1Error, L2Error, FindP, ListP, GradErrL1, HessErrL1];


Begin["`Private`"]


Clear[LinInterp];
LinInterp[x_,p_?VectorQ,f_]:=Module[{},
	Piecewise[
		Table[
			{(f[p[[i+1]]]-f[p[[i]]])/(p[[i+1]]-p[[i]])*(x-p[[i]])+f[p[[i]]], p[[i]]<x<=p[[i+1]]},
			{i,1,Length[p]-1}
		],
		f[x]
	]
];


Clear[L1Error];
L1Error[f_,p_?VectorQ]:=Module[{x,i,ErrInt,ErrTot},
	If[p!=Sort[p]||p!=DeleteDuplicates[p],
		Return[{Infinity,{Infinity}}],
		ErrInt=Table[
			{(p[[i+1]]+p[[i]])/2,
				NIntegrate[(f[p[[i+1]]]-f[p[[i]]])/(p[[i+1]]-p[[i]]) (x-p[[i]])+f[p[[i]]]-f[x],{x,p[[i]],p[[i+1]]}]},
			{i,1,Length[p]-1}
			];
		ErrTot=Total[ErrInt[[All,2]]];
		Return[{ErrTot,ErrInt}]
	]
];


Clear[L2Error];
L2Error[f_,p_?VectorQ]:=Module[{x,i,ErrInt,ErrTot},
	If[p!=Sort[p]||p!=DeleteDuplicates[p],
		Return[{Infinity,{Infinity}}],
		ErrInt=Table[
			{(p[[i+1]]+p[[i]])/2,
				NIntegrate[((f[p[[i+1]]]-f[p[[i]]])/(p[[i+1]]-p[[i]]) (x-p[[i]])+f[p[[i]]]-f[x])^2,{x,p[[i]],p[[i+1]]}]},
			{i,1,Length[p]-1}
			];
		ErrTot=Total[ErrInt[[All,2]]];
		Return[{Sqrt[ErrTot],Sqrt[ErrInt]}]
	]
];


Clear[FindP];
FindP[a_,b_,f_,fpI_,Num_?IntegerQ,iter_?IntegerQ]:=Module[{i,t,x,z},
	x=Table[i,{i,a,b,(b-a)/(Num+1)}];
	Do[
		For[i=2,i<=Num+1,i+=2,x[[i]]=N[fpI[(f[x[[i+1]]]-f[x[[i-1]]])/(x[[i+1]]-x[[i-1]])]]];
		For[i=3,i<=Num+1,i+=2,x[[i]]=N[fpI[(f[x[[i+1]]]-f[x[[i-1]]])/(x[[i+1]]-x[[i-1]])]]],
		{iter}
	];
	Return[x]
];


Clear[ListP];
ListP[p_?VectorQ,f_]:=Module[{i},
	Return[Table[{p[[i]],f[p[[i]]]},{i,1,Length[p]}]]
];
Protect[ListP];


Clear[GradErrL1];
GradErrL1[f_,p_?VectorQ]:=Module[{i},
	Return[
		Table[1/2*(f[p[[i-1]]]-f[p[[i+1]]]+(p[[i+1]]-p[[i-1]])*f'[p[[i]]]),
			{i,2,Length[p]-1}
		]
	]
];


Clear[HessErrL1];
HessErrL1[f_,p_?VectorQ]:=Module[{i,j,M},
	M=IdentityMatrix[Length[p]-2];
	If[Length[p]==3,
		M[[1,1]]=(p[[3]]-p[[1]])*f''[p[[2]]],
		M[[1,1]]=(p[[3]]-p[[1]])*f''[p[[2]]];
		M[[1,2]]=(f'[p[[2]]]-f'[p[[3]]]);
		For[i=2,i<=Length[p]-3,i++,
			M[[i,i-1]]=(f'[p[[i]]]-f'[p[[i+1]]]);
			M[[i,i]]=(p[[i+2]]-p[[i]])*f''[p[[i+1]]];
			M[[i,i+1]]=(f'[p[[i+1]]]-f'[p[[i+2]]]);
		];
		M[[Length[M],Length[M]-1]]=(f'[p[[Length[p]-2]]]-f'[p[[Length[p]-1]]]);
		M[[Length[M],Length[M]]]=(p[[Length[p]]]-p[[Length[p]-2]])*f''[p[[Length[p]-1]]];
	];
	Return[M/2];
];


End[]
SetAttributes[
{ LinInterp, L1Error, L2Error, FindP, ListP, GradErrL1, HessErrL1 },
{ Protected, ReadProtected }
];
EndPackage[]
